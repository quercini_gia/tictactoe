#!/usr/bin/python3
# -*- coding: utf-8 -*-

import asn1tools
import sys
import socket
import time
import argparse
from threading import Thread
import tkinter

### Global variables ###
# player is either 'X' or 'O', the value is set by received_by_server()
global player

# status is "wait" if the player is waiting for the second player "play" if the player has to play
global status

global protocol
global grid_canvas
global text_canvas
global client_socket
client_socket = None

##########


def get_user_inputs():
    # We parse the options, we have been passed by user, on command line (if any).
    default_IP = '127.0.0.1'
    default_port = 50001
    default_codec = 'ber'
    default_name = 'Client'
    protocol = "./TicTacToe.asn"
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", type=int,
                        help="Server port to connect to (default: %d)" % default_port)
    parser.add_argument(
        "-i", "--ip", help="Server ip address to connect to (default: %s)" % default_IP)
    parser.add_argument(
        "-c", "--codec", help="Message encoding: ber, der, gser, jer, oer, per, uper or xer (default: %s)" % default_codec)
    parser.add_argument(
        "-n", "--name", help="Player name (default: %s)" % default_name)
    args = parser.parse_args()
    if args.ip == None:
        args.ip = default_IP
    if args.port == None:
        args.port = default_port
    if args.codec == None:
        args.codec = default_codec
    if args.name == None:
        args.name = default_name

    return (args.name, args.ip, args.port, args.codec)


def draw_grid(grid_canvas):
    # draw horizontal lines
    x1 = 10
    x2 = 160
    for k in range(10, 200, 50):
        y1 = k
        y2 = k
        grid_canvas.create_line(x1, y1, x2, y2, width=3)
# draw vertical lines
    y1 = 10
    y2 = 160
    for k in range(10, 200, 50):
        x1 = k
        x2 = k
        grid_canvas.create_line(x1, y1, x2, y2, width=3)


def draw_X_color(grid_canvas, x0, y0, color):
    grid_canvas.create_line(20+y0*50, 20+x0*50, 50+y0*50,
                            50+x0*50, fill=color, width=6)
    grid_canvas.create_line(20+y0*50, 50+x0*50, 50+y0*50,
                            20+x0*50, fill=color, width=6)


def draw_O_color(grid_canvas, x0, y0, color):
    grid_canvas.create_oval(20+y0*50, 20+x0*50, 50+y0*50, 50+x0*50, width=6)


def get_case(event):
    if event.x > 20 and event.x < 70:
        coord_y = 0
    elif event.x > 70 and event.x < 130:
        coord_y = 1
    else:
        coord_y = 2
    if event.y > 20 and event.y < 70:
        coord_x = 0
    elif event.y > 70 and event.y < 130:
        coord_x = 1
    else:
        coord_x = 2
    return (coord_x, coord_y)


def draw_play(message_content):
    if player == 'X' and status == 'play':
        draw_X_color(
            grid_canvas, message_content['x'], message_content['y'], "black")
    elif player == 'X' and status == 'wait':
        draw_O_color(
            grid_canvas, message_content['x'], message_content['y'], "black")
    elif player == 'O' and status == 'play':
        draw_O_color(
            grid_canvas, message_content['x'], message_content['y'], "black")
    elif player == 'O' and status == 'wait':
        draw_X_color(
            grid_canvas, message_content['x'], message_content['y'], "black")


def on_click(event):
    global player
    global status
    (x, y) = get_case(event)
    if player == 'X':
        action = ('actionX', {'x': x, 'y': y})
    else:
        action = ('actionO', {'x': x, 'y': y})
    action_encoded = protocol.encode('Command', action)
    if status == 'play':
        client_socket.send(action_encoded)


def print_text(msg):
    global text_canvas
    text_canvas.delete("all")
    text_canvas.create_text(80, 20, text=msg, width=120)


def send_hello_to_server(name):
    print("sending Hello message to the server")
    hello = ('hello', {'name': name})
    hello_encoded = protocol.encode('Command', hello)
    client_socket.send(hello_encoded)


def received_by_server():
    """Handles receiving of messages."""
    global player
    global status
    status = 'wait'

    while True:
        try:
            if client_socket is not None:
                print("Waiting for message")
                data = client_socket.recv(1024)
                msg = protocol.decode('ServerAnswer', data)
                message_type = msg[0]
                message_content = msg[1]
                print("Message received %s %s" %
                      (message_type, message_content))
            if message_type == 'helloX':
                print_text("You are the first client")
                player = 'X'
            elif message_type == 'helloO':
                print_text("You are the second client")
                player = 'O'
            elif message_type == 'players':
                if player == 'X':
                    print_text("You are %s. \nYou play against %s." % (
                        message_content['playerX'], message_content['playerO']))

                else:
                    print_text("You are %s. \nYou play against %s." % (
                        message_content['playerO'], message_content['playerX']))
            elif message_type == "play":
                status = 'play'
                print_text(status)
            elif message_type == 'validPosition':
                draw_play(message_content)
                status = 'wait'
                print_text(status)
            elif message_type == 'invalidPosition':
                print_text("Wrong position")
            elif message_type == 'win':
                print_text("You win ! ")
            elif message_type == 'lose':
                print_text("You lose !")
            elif message_type == 'tie':
                print_text("Tie ...")
            else:
                print_text("Unknown message %s" % message_type)

        except OSError:
            break


def main():
    global protocol
    global grid_canvas
    global text_canvas
    global client_socket

    # User inputs
    (name, ip, port, codec) = get_user_inputs()
    protocol = asn1tools.compile_files('./TicTacToe.asn', codec)

    # Server connection
    # add here the code to connect the server
    # end of server connection
    client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    try:
        client_socket.connect((ip, port))
    except Exception as e:
        print("Problem with connection: ", e)
        exit(-1)

    print("Connection successful!")

    # Graphical interface
    # DO NOT MODIFY THIS PART
    root = tkinter.Tk()
    root.title('Tic Tac Toe')
    # create the grid canvas
    grid_canvas = tkinter.Canvas(root, width=170, height=170, bg='#95B2DD')
    grid_canvas.bind('<Button-1>', on_click)
    grid_canvas.pack()
    draw_grid(grid_canvas)
    text_canvas = tkinter.Canvas(root, width=170, height=70, bg="white")
    text_canvas.pack()
    # end of the graphical interface

    send_hello_to_server(name)

    receive_thread = Thread(target=received_by_server)
    receive_thread.start()

    tkinter.mainloop()


if __name__ == "__main__":
    # execute only if run as a script
    main()
