import os
import sys
import socket
import queue

# We need queue that can be polled by select() using a file descriptor.
# Trick found on:
# https://stackoverflow.com/questions/17495877/python-how-to-wait-on-both-queue-and-a-socket-on-same-time


class PollableQueue(queue.Queue):
    def __init__(self):
        super().__init__()
        # Create a pair of connected sockets
        print("Creation of  Unix sockets pair for "+os.name)
        if os.name == 'posix':
            self._putsocket, self._getsocket = socket.socketpair()
        else:
            # Compatibility on non-POSIX systems
            server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            server.bind(('127.0.0.1', 0))
            server.listen(1)
            self._putsocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self._putsocket.connect(server.getsockname())
            self._getsocket, _ = server.accept()
            server.close()

    def fileno(self):
        return self._getsocket.fileno()

    def put(self, item, block=True, timeout=None):
      super().put(item, block, timeout)
      self._putsocket.send(b'x')

    def get(self, block=True, timeout=None):
      if block:
        socket_timeout = timeout
      else:
        socket_timeout = 0
      self._getsocket.settimeout(socket_timeout)
      try:
        self._getsocket.recv(1)
      except socket.timeout as e:
        self._getsocket.settimeout(None)
        raise queue.Empty
      except BlockingIOError as e:
        self._getsocket.settimeout(None)
        raise queue.Empty
      self._getsocket.settimeout(None)
      try:
        msg = super().get(False)
      except Empty as e:
        print("This should not happen ...")
      return msg
