#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import queue
import asn1tools
import threading

class Decoder:
  def __init__(self, in_queue = None, out_queue = None, codec = 'ber', spec = "./TicTacToe.asn"):
    self.codec = codec
    self.keep_on_running = False
    self.spec = spec
    if in_queue == None:
      self.in_queue = queue.Queue()
    else:
      self.in_queue = in_queue
    if out_queue == None:
      self.out_queue = queue.Queue()
    else:
      self.out_queue = out_queue
    self.thread = None


  def start(self):
    self.keep_on_running = True
    self.thread = threading.Thread(target=self.process_data, args=[])
    self.thread.start()

  def stop(self):
    print("Programming decoder stop")
    self.keep_on_running = False
    # We give the opportunity the process loop to make a final trial on a fictious empty message.
    # Otherwise it may well be forerever locked on waiting on get method.
    self.in_queue.put(b'')
    self.thread.join()
    print("Decoder stopped")

  def put(self, data):
    self.in_queue.put(data)

  def process_data(self):
    print("Starting decoder %s %s %s" % (self.in_queue, self.out_queue, self.codec))
    try:
      protocol = asn1tools.compile_files(self.spec, codec=self.codec)
    except Exception as e:
      print("Erreur de compilation du fichier de spécification des messages du protocole")
      print(e)
      sys.exit(-1)

    data = b''

    while(True):
      # Si il n'y aucune données à recevoir du client et qu'on doit terminer, on sort.
      if(self.in_queue.empty() and not self.keep_on_running):
        return
      data = data + self.in_queue.get()
      print("Données reçues par le décodeur %s" % data)
      still_data_to_decode = True

      while still_data_to_decode:
        cmd = None
        try:
          cmd = protocol.decode('Command',data)
        except Exception as e:
          print("Impossible de décoder un message du protocole")
          print(e)

        # Si on a réussi à décoder une commande ...
        if(cmd != None):
          print("Evenement à traiter par l'automate %s" % cmd[0])
          self.out_queue.put((cmd[0], cmd[1]))
          # On calcule la longueur de données qui a été consommée
          try:
            length = protocol.decode_length(data)
          except asn1tools.DecodeError as e:
            # Si le codec ne supporte pas la méthode decode_length ...
            length = len(protocol.encode('Command', cmd))
          # On met à jour le début des données à décoder
          data = data[length:]
          # Si on n'a plus de données, on va en attendre de nouvelles
          if(len(data) == 0):
            still_data_to_decode = False
          # Sinon on va tenter de décoder un autre message
        else:
          # Si on n'a pas réussi à décoder un message, c'est qu'il faut attendre plus de données.
          still_data_to_decode = False

