#!/usr/bin/python3
# -*- coding: utf-8 -*-

from transitions_gui import WebMachine
# import threading
import select
import queue
from modules import pollablequeue
import sys


class MyStates:
    pass


class Automaton(WebMachine):
    def __init__(self, clients=[], states=[], initial=None, title="", before_state_change=[]):
        super().__init__(model=MyStates(), states=states, initial=initial,
                         before_state_change=before_state_change, queued=True, name=title)
        print("Creating an automaton for clients: %s" % clients)
        self.keep_on_running = False

        self.in_queues = {}
        self.out_queues = {}
        for client in clients:
            self.in_queues[client] = pollablequeue.PollableQueue()
            print("constructeur automate")
            self.out_queues[client] = queue.Queue()

        self.clients = clients

    def start(self):
        print("Starting automaton")
        self.keep_on_running = True
        #self.thread =  threading.Thread(target=self.run_automaton, args=[])
        # self.thread.start()
        self.run_automaton()

    def stop(self):
        print("Programming automaton stop")
        self.keep_on_running = False
        # self.thread.join()
        print("Automaton stopped")

    def no_more_events(self):
        res = True
        while client in self.clients:
            res = res and self.in_queues[client].empty()
        return res

    def run_automaton(self):
        queues = self.in_queues.values()
        inputs = []
        for q in queues:
            inputs.append(q)

        while True:
            if not self.keep_on_running and self.no_more_events():
                return

            select.select(inputs, [], [])

            # There is something to read
            for client in self.clients:
                # We read as much as possible (client by client)
                while not self.in_queues[client].empty():
                    event = self.in_queues[client].get()
                    cmd = event[0]
                    data = event[1]
                    print("Triggering event: %s with data: %s" % (cmd, data))
                    self.trigger(cmd, data)

        # There still may be event sent by other clients in the meantime.
        # We will detect these events in the next iteration
        print("Automaton stopped")

    def trigger(self, event, data=None):
        self.model.trigger(event, data)

    def on_enter(self, state, callback):
        if state in self.states:
            self.states[state].add_callback('enter', callback)
        else:
            print("State: %s does not exist. Cannot add a callback for it." % state)
            sys.exit(-1)

    def on_exit(self, state, callback):
        if state in self.states:
            self.states[state].add_callback('exit', callback)
        else:
            print("State: %s does not exist. Cannot add a callback for it." % state)
            sys.exit(-1)

    def emit(self, client, event):
        print("Preparing to send %s message to client %s" % (event, client))
        self.out_queues[client].put(event)


def add_transition(machine, event, source, destination, conditions=[], after=[], before=[]):
    print("adding a transition")
    machine.add_transition(event, source, destination,
                           conditions=conditions, after=after, before=before)


def on_enter(machine, state, callback):
    machine.on_enter(state, callback)


def on_exit(machine, state, callback):
    machine.on_exit(state, callback)


def emit(machine, client, event):
    machine.emit(client, event)


if __name__ == "__main__":
    main()
