# -*- coding: utf-8 -*-

import sys
import queue
import threading
import asn1tools

class Encoder:
  def __init__(self, in_queue = None, out_queue = None, codec = 'ber', spec = "./TicTacToe.asn"):
    self.spec = spec
    self.codec = codec
    self.keep_on_running = False
    if in_queue == None:
      self.in_queue = queue.Queue()
    else:
      self.in_queue = in_queue
    if out_queue == None:
      self.out_queue = queue.Queue()
    else:
      self.out_queue = out_queue
    self.thread = None

  def start(self):
    self.keep_on_running = True
    self.thread = threading.Thread(target=self.process_answers, args=[])
    self.thread.start()

  def stop(self):
    print("Programming encoder stop.")
    # We give the opportunity the process loop to make a final trial on a fictious empty message.
    # Otherwise it may well be forerever locked on waiting on get method.
    self.in_queue.put(None)
    self.keep_on_running = False
    self.thread.join()
    print("Encoder stopped.")

  def process_answers(self):
    print("Starting encoder %s %s %s" % (self.in_queue, self.out_queue, self.codec))
    try:
      protocol = asn1tools.compile_files(self.spec, codec=self.codec)
    except Exception as e:
      print("Erreur de compilation du fichier de spécification des messages du protocole")
      print(e)
      sys.exit(-1)

    while(True):
      if self.in_queue.empty() and not self.keep_on_running:
        break
      command = self.in_queue.get()
      try:
        data = protocol.encode('ServerAnswer', command)
        print("Encoder transmits data to server main loop")
        self.out_queue.put(data)
      except Exception as e:
        print("Impossible d'encoder un message du protocole")
        print(e)
