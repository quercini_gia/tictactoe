#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import socket
import select
import queue
import argparse
import time
import threading
from modules import automaton
from modules import pollablequeue
from modules import encoder
from modules import decoder

#### Global variables ###

server_automaton = None
K1 = 'Client1'
K2 = 'Client2'

# La liste (initialement vide) des sockets qui peuvent recevoir (potentiellement) des données.
inputs = []

# Un dictionnaire pour associer à chacun des deux clients son socket.
# La clé est le socket, et la valeur la chaîne de caractère: "Client1" ou "Client2".
clients = {}

# Name sent by clients.
# to be defined

# Les décodeurs ASN.1 associés à chaque client.
# La clé est le socket qui correspond à la connexion TCP et la valeur associée est un décodeur.
decoders = {}

# Idem pour les encodeurs ASN.1 associés à chaque client.
encoders = {}

# Automate du serveur
server_automaton = None

# Plateau de jeu initial
board = [['']*3 for i in range(3)]

# Number of nb_plays
nb_plays = 0

client_names = {}

#### Auxilliary functions ###

# Une fonction qui se charge de fermer proprement la connexion associée à une socket en la retirant de toutes les structure de contrôle.


def close_socket(s):
    print("Fermeture de la connexion: %s" % s)
    inputs.remove(s)
    if s in clients:
        client = clients[s]
        dec = decoders[s]
        enc = encoders[s]
        dec.stop()
        enc.stop()
        del decoders[s]
        del encoders[s]
        del clients[s]
    s.close()


def wait_for_connexion(ip, port, codec, spec):

    # On crée une nouvelle socket de type TCP
    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Permet de réutiliser la socket même en cas de mauvaise fermeture
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    # On la rend non bloquante.
    server_socket.setblocking(0)
    # On s'attache au port passé en paramètre sur l'adresse IP reçue en paramètre.
    server_socket.bind((ip, port))
    # On peut accepter une seule connexion simultanément.
    server_socket.listen(1)
    inputs.append(server_socket)

    # Tant qu'il y a une socket qui peut accepter une entrée ...
    while inputs:
        # On demande au système d'exploitation de nous réveiller quand il y a des données à lire, un évènement exceptionnel.
        #print("On entre dans select avec %s" % inputs)
        readable, _, exceptional = select.select(inputs, [], inputs)
        #print("On sort du select avec %s" % readable)

        # Pour chaque socket / queue sur laquelle on peut lire des données ...
        # print("Données en attente sur les sockets: %s, %s, %s" % (readable, exceptional))
        for s in readable:
            end_connection = False
            # S'il s'agit de la socket initiale, une donnée à lire indique en fait une demande de connexion (three-way handshake TCP)
            if s is server_socket:
                # Si on est encore en attente de clients:
                if len(clients) < 2:
                    # On accepte la connexion en provenance d'un client
                    connection, client_address = s.accept()
                    # On rend la socket non bloquante.
                    connection.setblocking(0)
                    if len(clients) == 0:
                        clients[connection] = K1
                    else:
                        clients[connection] = K2
                    # On ajoute cette nouvelle socket qui matérialise la connexion avec le client dans la liste des sockets qui peuvent recevoir des données ...
                    inputs.append(connection)
                    # On crée le décodeur pour cette connexion .
                    client = clients[connection]

                    pollqueue = server_automaton.in_queues[client]
                    dec = decoder.Decoder(
                        out_queue=pollqueue, codec=codec, spec=spec)
                    decoders[connection] = dec
                    print("Démarrage du décodeur pour le client %s" % client)
                    dec.start()

                    pollqueue = pollablequeue.PollableQueue()
                    inqueue = server_automaton.out_queues[client]
                    enc = encoder.Encoder(
                        in_queue=inqueue,  out_queue=pollqueue, codec=codec, spec=spec)
                    encoders[connection] = enc
                    inputs.append(pollqueue)
                    print("Démarrage de l'encodeur pour le client %s" % client)
                    enc.start()

                # On a déjà deux clients connectés
                else:
                    # On clôture cette socket.
                    close_socket(s)
            # Sinon il s'agit d'une socket qui matérialise une connexion déjà établie.
            elif s in clients:
                # On reçoit les données en provenance de la socket par morceau d'au plus 1024 octets.
                try:
                    data = s.recv(1024)
                    if data:
                        print("Données reçues: %s" % data)
                        # On ajoute ces données dans une file de messages dédiée à cette connexion.
                        decoders[s].put(data)
                        # S'il n'y a pas de données à lire, cela indique la fin programmée d'une connexion ...
                    else:
                        # On clôture la connexion.
                        close_socket(s)
                except ConnectionResetError as e:
                    print("Raccrochage violent de la connexion ...")
            else:
                # On doit avoir à affaire à une pollable queue
                print(s)
                for sock, enc in encoders.items():
                    try:
                        next_msg = enc.out_queue.get_nowait()
                    except queue.Empty as e:
                        pass
                    else:
                        # Sinon on émet le message en question sur la socket
                        print("Sending message %s on wire" % next_msg)
                        sock.send(next_msg)

        # Normalement le protocole ne supporte pas les données hors bande.
        # On arrête la connexion en cas de réception de telle données.
        for s in exceptional:
            close_socket(s)

# A function that displays board state on console


def display_board():
    for x in range(3):
        for y in range(3):
            print('|', end='')
            mark = board[x][y]
            if mark == '':
                print("_", end='')
            else:
                print(mark, end='')
        print("|\n")


def first_client(data):
    print("First client connected:" + str(data))
    client_names[K1] = data['name']
    server_automaton.emit(K1, ('helloX', {}))


def second_client(data):
    print("Second client connected:" + str(data))
    client_names[K2] = data['name']
    server_automaton.emit(K2, ('helloO', {}))
    server_automaton.trigger('tau', None)


def valid_name(data):
    return not invalid_name(data)


def invalid_name(data):
    return data['name'] == client_names[K1]


def send_invalid_name(data):
    server_automaton.emit(K2, ('invalidName', {}))


def play_X(data):
    time.sleep(1)
    server_automaton.emit(K1, ('play', {}))


def play_O(data):
    time.sleep(1)
    server_automaton.emit(K2, ('play', {}))


def tie(data):
    server_automaton.emit(K1, ('tie', {}))
    server_automaton.emit(K2, ('tie', {}))


def send_players(data):
    time.sleep(1)
    players = (
        'players', {'playerX': client_names[K1], 'playerO': client_names[K2]})

    server_automaton.emit(K1, players)
    server_automaton.emit(K2, players)


def send_valid_position_X(data):
    global nb_plays
    x = data['x']
    y = data['y']
    board[x][y] = 'X'
    print("sending last play to each client")
    display_board()
    time.sleep(1)
    server_automaton.emit(K1, ('validPosition', {'x': x, 'y': y}))
    server_automaton.emit(K2, ('validPosition', {'x': x, 'y': y}))
    nb_plays += 1


def send_valid_position_O(data):
    global nb_plays
    x = data['x']
    y = data['y']
    board[x][y] = 'O'
    print("sending last play to each client")
    display_board()
    time.sleep(1)
    server_automaton.emit(K1, ('validPosition', {'x': x, 'y': y}))
    server_automaton.emit(K2, ('validPosition', {'x': x, 'y': y}))
    nb_plays += 1


def send_invalid_position_X(data):
    server_automaton.emit(K1, ('invalidPosition', {}))


def send_invalid_position_O(data):
    server_automaton.emit(K2, ('invalidPosition', {}))


def send_win_lose(data):
    time.sleep(1)
    print("sending win/lose")
    _, who, what, where = who_is_winner()
    win = ('win', {'winningPosition': (what, where)})
    lose = ('lose', {'winningPosition': (what, where)})
    if who == 'X':
        server_automaton.emit(K1, win)
        server_automaton.emit(K2, lose)
        server_automaton.trigger('tau', None)
    elif who == 'O':
        server_automaton.emit(K2, win)
        server_automaton.emit(K1, lose)
        server_automaton.trigger('tau', None)


def send_tie(data):
    t = ('tie', {})
    server_automaton.emit(K1, t)
    server_automaton.emit(K2, t)
    server_automaton.trigger('tau', None)

# Récupérer de la correction du TD.


def valid_action(data):
    x = data['x']
    y = data['y']
    return board[x][y] == '' and not who_is_winner()[0]


def invalid_action(data):
    x = data['x']
    y = data['y']
    return board[x][y] == 'X' or board[x][y] == 'O'


def is_tie(data):
    return nb_plays == 9 and not who_is_winner()[0]


def play(data):
    return nb_plays < 9 and not who_is_winner()[0]


def who_is_winner():
    for line in range(0, 2):
        who = board[line][0]
        winner = (who == board[line][1]) and (
            who == board[line][2]) and not who == ''
        if winner:
            return (True, who, 'row', line)
    for column in range(0, 2):
        who = board[0][column]
        winner = (who == board[1][column]) and (
            who == board[2][column]) and not who == ''
        if winner:
            return (True, who, 'column', column)
    who = board[0][0]
    winner = (who == board[1][1]) and (who == board[2][2]) and not who == ''
    if winner:
        return (True, who, 'diagonal', 1)
    who = board[2][0]
    winner = (who == board[1][1]) and (who == board[0][2]) and not who == ''
    if winner:
        return (True, who, 'diagonal', -1)
    return (False, None, None, None)


def win_or_lose(data):
    return who_is_winner()[0]


def go_on(data):
    server_automaton.trigger('tau', None)


def create_automaton():
    # creation of the automaton

    # The states of the automaton (to be completed)

    states = ['init', 'client X', 'client O', 'wait X', 'wait O',
              'situation X', 'situation O', 'tie', 'win', 'end']

    # The automaton itself
    autom = automaton.Automaton(
        clients=[K1, K2],
        states=states,
        initial='init')

    # Transitions of the automaton (to be completed)
    #automaton.on_enter(autom, 'client X', first_client)
    automaton.on_enter(autom, 'client O', second_client)
    automaton.on_exit(autom, 'client O', send_players)
    automaton.on_enter(autom, 'wait X', play_X)
    automaton.on_enter(autom, 'wait O', play_O)
    automaton.on_enter(autom, 'tie', tie)
    automaton.on_enter(autom, 'win', send_win_lose)
    automaton.on_enter(autom, 'situation X', go_on)
    automaton.on_enter(autom, 'situation O', go_on)

    automaton.add_transition(autom, 'hello', 'init',
                             'client X', before=first_client)

    automaton.add_transition(autom, 'hello', 'client X',
                             'client O', conditions=[valid_name])

    automaton.add_transition(autom, 'hello', 'client X',
                             'client X', conditions=[invalid_name], before=send_invalid_name)

    automaton.add_transition(autom, 'tau', 'client O', 'wait X')

    automaton.add_transition(autom, 'actionX', 'wait X', 'situation X', conditions=[
                             valid_action], before=send_valid_position_X)

    automaton.add_transition(autom, 'actionX', 'wait X', 'wait X', conditions=[
                             invalid_action], before=send_invalid_position_X)

    automaton.add_transition(autom, 'actionO', 'wait O', 'situation O', conditions=[
                             valid_action], before=send_valid_position_O)

    automaton.add_transition(autom, 'actionO', 'wait O', 'wait O', conditions=[
                             invalid_action], before=send_invalid_position_O)

    automaton.add_transition(autom, 'tau', 'situation X',
                             'tie', conditions=[is_tie])

    automaton.add_transition(autom, 'tau', 'situation X',
                             'win', conditions=[win_or_lose])

    automaton.add_transition(autom, 'tau', 'situation X',
                             'wait O', conditions=[play])

    automaton.add_transition(autom, 'tau', 'situation O',
                             'tie', conditions=[is_tie])

    automaton.add_transition(autom, 'tau', 'situation O',
                             'win', conditions=[win_or_lose])

    automaton.add_transition(autom, 'tau', 'situation O',
                             'wait X', conditions=[play])

    automaton.add_transition(autom, 'tau', 'tie', 'end')
    automaton.add_transition(autom, 'tau', 'win', 'end')

    return autom


def get_user_input():
    parser = argparse.ArgumentParser()
    parser.add_argument("-p", "--port", type=int, help="port to bind to")
    parser.add_argument("-i", "--ip", help="port to bind to")
    parser.add_argument(
        "-c", "--codec", help="Message encoding: ber, der, gser, jer, oer, per, uper or xer. BER is the default value.")
    parser.add_argument(
        "-s", "--spec", help="Protocole specification file in ASN.1")

    args = parser.parse_args()

    if args.ip is None:
        args.ip = '127.0.0.1'
    if args.port is None:
        args.port = 50001
    if args.codec is None:
        args.codec = 'ber'
    if args.spec is None:
        args.spec = "./TicTacToe.asn"

    return (args.ip, args.port, args.codec, args.spec)


def main():
    global server_automaton
    (ip, port, codec, spec) = get_user_input()

    server_automaton = create_automaton()

    network_thread = threading.Thread(
        target=wait_for_connexion, args=[ip, port, codec, spec])
    network_thread.start()

    # start the automaton
    server_automaton.start()


if __name__ == "__main__":
    # execute only if run as a script
    main()
